#include <stdio.h>
#include <string.h>

int main(int argc, char* argv[])
{
	if (argc < 2) {
		fprintf(stderr, "Too few arguments\n");
		goto err_arg_count;
	}

	if (strcmp(argv[1], "--asdf") != 0) {
		fprintf(stderr, "Invalid argument");
		goto err_not_asdf;
	}
	return 0;
err_not_asdf:
err_arg_count:
	return 0;
}
