#include "clang/Frontend/FrontendActions.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
// Declares llvm::cl::extrahelp.
#include "llvm/Support/CommandLine.h"
#include <clang/ASTMatchers/ASTMatchFinder.h>

using namespace clang::tooling;
using namespace llvm;
using namespace clang::ast_matchers;

// CommonOptionsParser declares HelpMessage with a description of the common
// command-line options related to the compilation database and input files.
// It's nice to have this help message in all tools.
static cl::extrahelp CommonHelp(CommonOptionsParser::HelpMessage);

// A help message for this specific tool can be added afterwards.
static cl::extrahelp MoreHelp("\nMore help text...");

class HandleMatch : public MatchFinder::MatchCallback
{
public:
virtual void run(const MatchFinder::MatchResult &Result) {
	const clang::Stmt* stmt = Result.Nodes.getDeclAs<clang::Stmt>("stmt");
	stmt->dump();
}
};

int main(int argc, const char **argv) {
  CommonOptionsParser OptionsParser(argc, argv);
  ClangTool Tool(OptionsParser.GetCompilations(),
                 OptionsParser.GetSourcePathList());
  MatchFinder mf;
  mf.addMatcher(stmt().bind("stmt"), new HandleMatch());
  return Tool.run(newFrontendActionFactory(&mf));
}
