LLVM=llvm_install

CXX=$(LLVM)/bin/clang++

CXXFLAGS=$(shell $(LLVM)/bin/llvm-config --cxxflags) -Wall -Wextra -Werror -fno-diagnostics-show-option -Wno-unused-parameter

LDFLAGS=\
	-lclangASTMatchers -lclangFrontend -lclangSerialization -lclangDriver -lclangTooling \
	-lclangParse -lclangSema -lclangAnalysis -lclangEdit -lclangAST \
	-lclangLex -lclangBasic $(shell $(LLVM)/bin/llvm-config --ldflags) \
	$(shell $(LLVM)/bin/llvm-config --libs) 

CFLAGS=-Wall -Wextra -Werror

all: goto_checker goto

clean:
	-rm goto_checker goto_checker.o
